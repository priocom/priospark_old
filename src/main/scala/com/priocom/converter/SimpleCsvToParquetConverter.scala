package com.priocom.converter

import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by jcrm on 23.03.16.
 */
object SimpleCsvToParquetConverter {

  def main(args: Array[String]) {
    val logFile = "hdfs://192.168.128.16:8020/user/ks/ggsn.csv"
    val conf = new SparkConf().setAppName("Simple Application").setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqLContext = new SQLContext(sc)

    val schema= StructType(Array(
      StructField("CHARGING_ID",            StringType,true),
      StructField("NODEID",                 StringType,true),
      StructField("SEQUENCE_NUMBER",        StringType,true),
      StructField("MSISDN",                 StringType,true),
      StructField("IMSI",                   StringType,true),
      StructField("OPENING_TIME",           DateType,true),
      StructField("SERVED_IP_ADDRESS",      StringType,true),
      StructField("SGSNADDRESS",            StringType,true),
      StructField("GGSNADDRESS",            StringType,true),
      StructField("APN",                    StringType,true),
      StructField("DURATION",               IntegerType,true),
      StructField("CHARGING_CHARACTERISTICS", StringType,true),
      StructField("DATA_VOLUME_UPLINK",     IntegerType,true),
      StructField("DATA_VOLUME_DOWNLINK",   IntegerType,true),
      StructField("CAUSE_FOR_RECCLOSING",   StringType,true),
      StructField("ISROAMING",              IntegerType,true),
      StructField("FILENAME",               StringType,true),
      StructField("IMEISV",                 StringType,true),
      StructField("NUMBER_OF_PART",         IntegerType,true),
      StructField("RATING_GROUP",           IntegerType,true),
      StructField("DATA_VOLUME_RG_UPLINK",  IntegerType,true),
      StructField("DATA_VOLUME_RG_DOWNLINK", IntegerType,true),
      StructField("RESULT_CODE",            IntegerType,true),
      StructField("RATTYPE",                IntegerType,true),
      StructField("GSNPLMNIDENTIFIER",      StringType,true),
      StructField("LAC",                    StringType,true),
      StructField("CELL",                   StringType,true),
      StructField("TIMEOFFIRSTUSAGE",       DateType,true)
    ))

    convert(sqLContext, schema, "hdfs://192.168.128.16:8020/user/ks/ggsn.csv", "test3", "hdfs://192.168.128.16:8020/user/ks/prqt/")
  }

  def convert(sqlContext: SQLContext, schema: StructType, filename: String, tablename: String, dirOut: String) {
    val df = sqlContext.read
      .format("com.databricks.spark.csv")
      .schema(schema)
      .option("delimiter", "|")
      .option("nullValue", "")
      .option("treatEmptyValuesAsNulls", "true")
      .load(filename)

    df.write.parquet(dirOut + tablename)
  }
}
