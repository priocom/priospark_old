name := "priospark"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies += s"org.apache.spark" %% "spark-core" % "1.6.1"
libraryDependencies += "org.apache.spark" % "spark-sql_2.10" % "1.6.0"
libraryDependencies += "com.databricks" % "spark-csv_2.10" % "1.3.1"